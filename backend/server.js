// USE EXPRESS
const express = require('express');
const app = express();
app.use(express.json());

// USE MONGOOSE
const mongoose = require('mongoose');

// USE CORS
const cors = require('cors');
app.use(express.urlencoded({extended:false}));
app.use(cors());

// CONNECTING TO DATABASE IN MONGODB
const databaseUrl = process.env.DATABASE_URL || "mongodb+srv://Admin:admin@gerald-tcwtz.mongodb.net/myCureNotesApp?retryWrites=true&w=majority"
mongoose.connect(databaseUrl, {useUnifiedTopology:true, useCreateIndex:true, useNewUrlParser: true, useFindAndModify: false }).then(()=>{
    console.log("Remote Database Connection Established");
});

// USE config.js
const config = require('./config');
// **** REVIEW THIS LATER
app.listen(config.port, ()=>{
    console.log(`Listening On Port ${config.port}`);
})


// API's for NOTES, USERS AND AUTHENTICATION
const note = require('./routes/note_router');
app.use('/', note);

const users = require('./routes/user_router');
app.use('/', users);

const auth = require('./routes/auth_router');
app.use('/', auth);