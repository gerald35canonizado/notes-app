const express = require('express');

const NoteRouter = express.Router();

const NoteModel = require("../models/Note");

// ADDING NOTES
NoteRouter.post('/addnote', async (req, res)=> {
    try {
        let note = NoteModel ({
            title : req.body.title,
            content : req.body.content,
            userId : req.body.userId
        });
        note = await note.save();
        res.send(note);
    } catch (e) {
        console.log(e)
    }
});

// SHOWING ALL NOTES
NoteRouter.get('/shownote', async (req, res)=>{
    try {
        let note = await NoteModel.find();
        res.send(note);
    } catch (e) {
        console.log(e)
    }
});

// SHOWING NOTES BY ID
NoteRouter.get('/shownotebyid/:id', async(req, res)=>{
    try {
        let note =  await NoteModel.findById(req.params.id);
        res.send(note)
    } catch (e) {
        console.log(e)
    }
});

// UPDATING NOTE STATUS
NoteRouter.patch('/updatecompletedbyid/:id', async (req,res)=>{
    try {
        let condition = {_id: req.params.id};
        let update = {
            completed: req.body.completed
        }
        let updateCompleted = await NoteModel.findOneAndUpdate(condition, update, {new:true});
        res.send(updateCompleted)
    } catch (e) {
        res.status(400).send('error')
        console.log(e)
    }
})

// UPDATING NOTES BY ID
NoteRouter.put('/updatenotebyid/:id', async (req, res) => {
    try {
        let note = await NoteModel.findById(req.params.id);

        // IF THERE IS NO NOTE !note
        if(!note){
            return res
            .status(404)
            .send(`Note can't be found`);
        }
        let condition = {_id: req.params.id};
        let update = {
            title : req.body.title,
            content : req.body.content
        }
        let updatedNote = await NoteModel.findOneAndUpdate(condition, update, {new:true});
        res.send(updatedNote);
    } catch (e) {
        console.log(e)
    }
})

// DELETING SAVED NOTE BY ID
NoteRouter.delete('/deletenotebyid/:id', async (req, res) => {
    try {
        let deleteNote = await NoteModel.findByIdAndDelete(req.params.id);
        res.send(deleteNote);
    } catch (e) {
        console.log(e)
    }
})

// SHOWING NOTE BY USER
NoteRouter.get('/shownotebyuser/:id', async (req, res)=>{
	let note = await NoteModel.find({userId:req.params.id});
	res.send(note);
});
module.exports = NoteRouter;