const mongoose = require ('mongoose');

const Schema = mongoose.Schema;

const NoteSchema = new Schema ({
    title : String,
    content : String,
    completed : {
		type: Boolean,
		default: false
	},
    userId : String
})
module.exports = mongoose.model("Note", NoteSchema);